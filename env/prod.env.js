'use strict'
module.exports = {
  NODE_ENV: '"production"',
  HOST: '"' + process.env.HOST + '"',
  chatHost: '"' + process.env.chatHost + '"',
  chatApi: '"' + process.env.chatApi + '"',
  SITE_TITLE: '"' + process.env.SITE_TITLE + '"',
  HTTPS: '"' + process.env.HTTPS + '"',
  eiderHost: '"' + process.env.eiderHost + '"',
  eagleHost: '"' + process.env.eagleHost + '"',
  donaldHost: '"' + process.env.donaldHost + '"',
  foxHost: '"' + process.env.foxHost + '"',
  wsEagleHost: '"' + process.env.wsEagleHost + '"',
  venomHost: '"' + process.env.venomHost + '"',
  venomSocket: '"' + process.env.venomSocket + '"',
  company: '"' + process.env.company + '"'
}
