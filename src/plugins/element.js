import Vue from 'vue'
import {
  Button,
  DatePicker,
  Pagination,
  Radio,
  Checkbox
} from 'element-ui'
import lang from 'element-ui/lib/locale/lang/zh-TW'
import locale from 'element-ui/lib/locale'

locale.use(lang)

Vue.use(Button)
Vue.use(DatePicker)
Vue.use(Pagination)
Vue.use(Radio)
Vue.use(Checkbox)
