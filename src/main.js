import Vue from 'vue'
import App from './App.vue'
import store from './store'
import { router } from './router'
import { sync } from 'vuex-router-sync'
import { setRuntimeVM } from './hooks/initRuntime'
import '@/api'
import './plugins'
import * as watcher from './watcher'
import { dispatch } from './watcher/helper'

const isDebugMode = process.env.NODE_ENV !== 'production'
Vue.config.debug = isDebugMode
Vue.config.devtools = isDebugMode

sync(store, router)
dispatch('game/fetch', 'app/fetchSystemConfig', 'announcement/fetch')
if (store.state.token.ghost) {
  dispatch('user/fetch').catch(() => {})
} else {
  Vue.nextTick(() => {
    dispatch('user/reset')
    /* ====測試用==== */
    store.dispatch('user/login', {
      user: {
        username: 'tim222',
        password: 'tim222',
        verification_code_0: '',
        verification_code_1: ''
      }
    }).then(res => {
      console.log('login', res)
    })
    /* ============= */
  })
}

// store watch
watcher.watchUserLoginStatus()
// 動態更新router
watcher.setDynamicRouting()

Vue.mixin({ beforeCreate: setRuntimeVM })

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
