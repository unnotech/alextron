import { ref, watch, onBeforeMount, onBeforeUnmount, toRefs } from '@vue/composition-api'
import { addClass, removeClass, hasClass } from '@/utils/dom'
import PopupManager from './popup-manager'
import Vue from 'vue'

let idSeed = 1
let scrollTop

export default function usePopup (props) {
  const _popupId = ref('')
  const opened = ref(false)
  const _opening = ref(false)
  const bodyPaddingRight = ref(null)
  const computedBodyPaddingRight = ref(0)
  const withoutHiddenClass = ref(true)
  const rendered = ref(false)
  const _closeTimer = ref(null)
  const _openTimer = ref(null)
  const _closing = ref(false)
  const {
    visible,
    openDelay,
    closeDelay,
    zIndex,
    modal,
    modalAppendToBody,
    modalClass,
    modalFade,
    onOpen = ref(null),
    onClose = ref(null),
    lockScroll
  } = toRefs(props)

  const dom = ref(null)

  watch(visible, (val) => {
    if (val) {
      if (_opening.value) return
      if (!rendered.value) {
        rendered.value = true
        Vue.nextTick(() => {
          open()
        })
      } else {
        open()
      }
    } else {
      close()
    }
  }, { lazy: true })

  onBeforeMount(() => {
    _popupId.value = 'popup-' + idSeed++
    PopupManager.register(_popupId.value, this)
  })

  onBeforeUnmount(() => {
    PopupManager.deregister(_popupId.value)
    PopupManager.closeModal(_popupId.value)

    restoreBodyStyle()
  })

  const open = (options) => {
    if (!rendered.value) {
      rendered.value = true
    }

    if (_closeTimer.value) {
      clearTimeout(_closeTimer.value)
      _closeTimer.value = null
    }
    clearTimeout(_openTimer.value)

    const delayNum = Number(openDelay.value)
    if (delayNum > 0) {
      _openTimer.value = setTimeout(() => {
        _openTimer.value = null
        doOpen()
      }, delayNum)
    } else {
      doOpen()
    }
  }

  const doOpen = (props) => {
    if (Vue.prototype.$isServer) return
    if (opened.value) return

    _opening.value = true

    if (zIndex.value) {
      PopupManager.zIndex = zIndex.valued
    }

    if (modal.value) {
      if (_closing.value) {
        PopupManager.closeModal(_popupId.value)
        _closing.value = false
      }
      PopupManager.openModal(_popupId.value, PopupManager.nextZIndex(), modalAppendToBody.value ? undefined : dom, modalClass.value, modalFade.value)
      if (lockScroll.value) {
        // 在弹出层显示之前，记录当前的滚动位置
        scrollTop = document.body.scrollTop || document.documentElement.scrollTop

        withoutHiddenClass.value = !hasClass(document.body, 'popup-parent--hidden')
        addClass(document.body, 'popup-parent--hidden')

        // 把脱离文档流的body拉上去，否则页面会回到顶部
        document.body.style.top = -scrollTop + 'px'
      }
    }

    if (getComputedStyle(dom.value).position === 'static') {
      dom.value.style.position = 'absolute'
    }

    dom.value.style.zIndex = PopupManager.nextZIndex()
    opened.value = true
    onOpen.value && onOpen.value()

    doAfterOpen()
  }

  const doAfterOpen = () => {
    _opening.value = false
  }

  const close = () => {
    if (_openTimer.value !== null) {
      clearTimeout(_openTimer.value)
      _openTimer.value = null
    }
    clearTimeout(_closeTimer.value)

    const delayNum = Number(closeDelay.value)
    if (delayNum > 0) {
      _closeTimer.value = setTimeout(() => {
        _closeTimer.value = null
        doClose()
      }, delayNum)
    } else {
      doClose()
    }
  }

  const doClose = () => {
    _closing.value = true

    onClose.value && onClose.value()

    if (lockScroll.value) {
      setTimeout(restoreBodyStyle.value, 200)
    }

    opened.value = false

    doAfterClose()
  }

  const doAfterClose = () => {
    PopupManager.closeModal(_popupId.value)
    _closing.value = false
  }

  const restoreBodyStyle = () => {
    if (modal.value && withoutHiddenClass.value) {
      document.body.style.paddingRight = bodyPaddingRight.value
      removeClass(document.body, 'popup-parent--hidden')
      // body又回到了文档流中
      document.body.scrollTop = document.documentElement.scrollTop = scrollTop
    }
    withoutHiddenClass.value = true
  }

  return {
    visible,
    opened,
    bodyPaddingRight,
    computedBodyPaddingRight,
    withoutHiddenClass,
    rendered,
    open,
    dom
  }
}
