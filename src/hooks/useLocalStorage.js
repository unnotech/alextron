import { ref, onMounted, onUnmounted, watch } from '@vue/composition-api'

function parseValue (serializedValue) {
  let value = null
  try {
    // 如果是JSON格式
    value = JSON.parse(serializedValue)
  } catch {
    value = serializedValue
  }
  return value
}

/**
 * LocalStorage改變時分頁也會同步更新數據
 * @param {*} key // key
 * @param {*} def //預設值 undefined
 * @param {*} readOnly //只響應值的變化
 */
export default function useLocalStorage (key, def, readOnly = false) {
  const value = ref(null)

  const init = () => {
    const serializedValue = localStorage.getItem(key)
    // localStorage 有存就初始化值
    if (serializedValue !== null) {
      value.value = parseValue(serializedValue)
      return
    }

    // 否則直接等於預設值
    value.value = def
  }

  // 初始化flag
  let initialized = false

  // eslint-disable-next-line valid-typeof
  if (typeof window !== undefined) {
    init()
    initialized = true
  }

  function handler (event) {
    if (event.key === key) {
      value.value = event.newValue ? parseValue(event.newValue) : null
    }
  }

  watch(
    value, () => {
      if (!readOnly) {
        localStorage.setItem(key, JSON.stringify(value.value))
      }
    },
    {
      lazy: true
    }
  )

  onMounted(() => {
    if (!initialized) {
      init()
    }

    window.addEventListener('storage', handler, true)
  })

  onUnmounted(() => {
    localStorage.setItem(key, JSON.stringify(value.value))
    window.removeEventListener('storage', handler)
  })

  return {
    value
  }
}

/**
  範例
   <template>
    <div>
      {{ value }}
      <input v-model="text">
      <button @click="value = text">change</button>
    </div>
  </template>

  <script>
  import { ref } from '@vue/composition-api'
  import { useLocalStorage } from '@/hooks'

  export default {
    setup() {
      const { value } = useLocalStorage('test')
      const text = ref('')

      return {
        value,
        text
      }
    }
  }
  </script>
 */
