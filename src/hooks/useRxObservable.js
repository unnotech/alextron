import { onMounted, onUnmounted, ref, reactive } from '@vue/composition-api'

/**
 * 不用再處理生命週期的部分，自行決定subscribe執行的時機
 * @param {function} observableFunc
 * @returns {function} subscribe function
 */
/* 範例
<script>
import { useRxObservable } from '@/hooks'
import { fromEvent } from 'rxjs'
import { takeUntil, map, concatAll } from 'rxjs/operators'

export default {
  setup (props, { refs }) {
    const { subscribe } = useRxObservable(() => {
      const mouseMove = fromEvent(document.body, 'mousemove')
      const mouseUp = fromEvent(document.body, 'mouseup')
      const mousedown = fromEvent(refs.drag, 'mousedown')

      return mousedown.pipe(
        map(_ => mouseMove.pipe(takeUntil(mouseUp))),
        concatAll(),
        map(event => ({ x: event.clientX, y: event.clientY }))
      )
    })

    subscribe(v => {
      refs.links.style.width = v.x + 'px'
    })
  }
}
</script>
*/
export default function useRxObservable (observableFunc) {
  const observable$ = ref(null)
  const subscribe = ref(null)
  const event = reactive({
    subscribe: fn => {
      subscribe.value = e => fn(e)
    }
  })

  if (!observableFunc) {
    throw new Error(
      `[useRxObservable]: 1st argument is required (must be a function)`
    )
  }

  if (typeof observableFunc !== 'function') {
    throw new Error(
      `[useRxObservable]: argument has to be function, but received ${typeof observableFunc}`
    )
  }

  onMounted(() => {
    observable$.value = observableFunc()

    observable$.value.subscribe(subscribe.value)
  })

  onUnmounted(() => {
    if (observable$.value !== undefined) {
      observable$.value.unsubscribe()
    }
  })

  return {
    subscribe: event.subscribe
  }
}
