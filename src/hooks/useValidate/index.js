import { ref, onMounted, onBeforeUnmount } from '@vue/composition-api'
import { getRuntimeVM } from '../initRuntime'
import validateField from './validateField'

/**
   USAGE
  @param {String} mode default: onSubmit
    onSubmit 提交驗證
    onBlur blur事件觸發驗證
  @example
    const {register} = useValidate({mode: 'onBlur'})

    提供四個function
    const {register, errors, reset, handleSubmit} = useValidate()

  @description register 會回傳一個 ref object, 可用ref template方式綁在dom元素上

    提供幾種基本檢核
      * required 必填
        範例：
          const input = register() 預設檢查必填

          const input = register({ 預設提供錯誤訊息
            required: true
          })

          const input = register({ 自定義錯誤訊息
            required: '此欄位就是必填'
          })

      * min 數字最小值 （注意input type="number" 才有效）
      * max 數字最大值 （注意input type="number" 才有效）
      * minLength 最小長度
      * maxLength 最大長度
        範例：
          預設提供錯誤訊息
          const input = register({
            min: 1,
            minLength: 10
          })

          自定義錯誤訊息
          const input = register({
            min: {value: 1, message: '最少為一個'},
            minLength: {value: 1, message: '最少為一個字元'}
          })

      * pattern 自訂Regex檢核
        範例：
          const input = register({
            pattern: {
              value: /[A-Za-z]{3}/,
              message: '檢核失敗！'
            }
          })

  使用例子：
    注意 input name 為必填
    errors 為一個object，若有檢核錯誤的欄位，用errors[檢核錯誤的欄位的name].message 取得錯誤訊息
    handleSubmit 為一個Promise 會回傳boolean值，會檢查所有register的欄位是否通過檢核
    reset 重置檢核

    <template>
      <input name="user" ref="input" value="" />
      <div v-if="errors.user">{{ errors.user.message }}</div>

      <button @click="submit">submit</button>
      <button @click="reset">reset</button>
    </template>
    ...
    setup(){
      const {register, errors, handleSubmit, reset} = useValidate({mode: 'onBlur'})
      const input = register({
        required: '此欄位為必填！！！',
        minLength: 10,
        maxLength: { value: 100, message: '最多就是100' }
      })

      const submit = async () => {
        const res = await handleSubmit()
        if(res) {
          // todo
        }
      }
      return {
        input,
        submit,
        reset
      }
    }

 */

export default function useValidate ({ mode = 'onSubmit' }) {
  const vm = getRuntimeVM()
  const errors = ref({})
  const fieldsWithValidationRef = ref(null)
  const fieldsWithValidationRules = ref(null)

  const register = (rules = { required: true }) => {
    const refElement = ref(null)

    vm.$nextTick(() => {
      const { name } = refElement.value
      if (!name) {
        return console.warn('Missing name on ref', refElement)
      }
      fieldsWithValidationRef.value.add(name)
      fieldsWithValidationRules.value.set(name, rules)
      if (mode === 'onBlur') {
        refElement.value.addEventListener('blur', () => validateSingleField(refElement.value, rules))
      }
    })

    return refElement
  }

  const handleSubmit = async () => {
    reset()
    const queue = []
    Object.keys(vm.$refs)
      .filter(key => fieldsWithValidationRef.value.has(key))
      .forEach(async key => {
        queue.push(
          validateField(
            vm.$refs[key],
            fieldsWithValidationRules.value.get(key)
          )
        )
      })

    const resolved = await Promise.all(queue)

    errors.value = resolved.reduce((acc, cur, i) => ({ ...acc, ...cur }), {})

    return Object.entries(errors.value).length === 0
  }

  const validateSingleField = async (ref, rules) => {
    const result = await validateField(ref, rules)
    errors.value = { ...errors.value, ...result }
  }

  const reset = () => {
    errors.value = {}
  }

  onMounted(() => {
    fieldsWithValidationRef.value = new Set()
    fieldsWithValidationRules.value = new Map()
  })

  onBeforeUnmount(() => {
    if (mode === 'onBlur') {
      Object.keys(vm.$refs)
        .filter(key => fieldsWithValidationRef.value.has(key))
        .forEach(async key => {
          vm.$refs[key].removeEventListener(
            'blur',
            () => validateSingleField(vm.$refs[key], fieldsWithValidationRules.value.get(key))
          )
        })
    }
  })

  return {
    register,
    errors,
    reset,
    handleSubmit
  }
}
