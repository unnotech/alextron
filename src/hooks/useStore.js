import { computed } from '@vue/composition-api'
import { getRuntimeVM } from './initRuntime'

/** Usage:

    import { createComponent, computed } from '@vue/composition-api';
    import { useStore } from '@/hooks';

    const Demo = createComponent({
      setup() {
        const store = useStore();
        const plusOne = computed(() => store.value.state.count + 1);

        const increment = () => store.value.commit('increment');
        const incrementAsync = () => store.value.dispatch('incrementAsync');

        return { store, plusOne, increment, incrementAsync };
      },

      render() {
        const { store, plusOne } = this;
        return (
          <div>
            <div>count: {store.state.count}</div>
            <div>plusOne: {plusOne}</div>
            <button onClick={this.increment}>Increment</button>
            <button onClick={this.incrementAsync}>Increment Async</button>
          </div>
        );
      },
    });
 */

export default function useStore () {
  const vm = getRuntimeVM()
  const store = computed(() => vm.$store)
  return store
}
