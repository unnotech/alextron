export default [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/screens/Home/index')
  },
  {
    path: '/',
    name: 'Entry',
    component: () => import('@/screens/Entry/index'),
    children: [
      {
        path: 'login',
        name: 'Login',
        meta: { layout: 'simplify' },
        component: () => import('@/screens/Entry/components/LoginForm')
      },
      {
        path: 'register-success',
        name: 'RegisterSuccess',
        meta: { layout: 'simplify' },
        component: () => import('@/screens/Entry/components/RegisterSuccess')
      },
      {
        path: 'user-agreement',
        name: 'UserAgreement',
        meta: { layout: 'simplify' },
        component: () => import('@/screens/Entry/components/UserAgreement')
      }
    ]
  }
]
