export default [
  {
    path: '/game',
    name: 'GameHall',
    component: () => import('@/screens/GameHall/index'),
    children: [{
      path: ':gameId',
      name: 'Game',
      component: () => import('@/screens/GameHall/Game/index'),
      children: [
        {
          path: 'history',
          name: 'History',
          component: () => import('@/screens/GameHall/Game/History/index')
        },
        {
          path: 'trend',
          name: 'Trend',
          component: () => import('@/screens/GameHall/Game/Trend/index')
        },
        {
          path: 'accumulation-statistic',
          name: 'AccumulationStatistic',
          component: () => import('@/screens/GameHall/Game/AccumulationStatistic/index')
        },
        {
          path: 'accumulation-trend',
          name: 'AccumulationTrend',
          component: () => import('@/screens/GameHall/Game/AccumulationTrend/index')
        },
        {
          path: ':categoryId',
          name: 'GameCategory',
          component: () => import('@/screens/GameHall/Game/GameCategory/index')
        }
      ]
    }]
  }
]
