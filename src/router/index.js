import Vue from 'vue'
import Router from 'vue-router'
import entry from './modules/entry'
import game from './modules/game'

Vue.use(Router)

export const router = new Router({
  route: []
})

const baseRoutes = [
  {
    path: '*',
    redirect: '/'
  }
]

const authFilter = (routes) => {
  const result = []
  routes.forEach(route => {
    let copyRoute = { ...route } // 為了不破壞原先的結構，故拷貝一份
    if (!copyRoute.meta || !copyRoute.meta.requiresAuth) {
      result.push(copyRoute)
      if (copyRoute.children) {
        copyRoute.children = authFilter(copyRoute.children)
      }
    }
  })
  return result
}

export function refreshRouter ({ user, theme }) {
  if (!theme) {
    return
  }
  const newRouter = new Router({ route: [] })
  router.matcher = newRouter.matcher
  let newRoutes = [
    ...baseRoutes,
    ...entry,
    ...game
  ]
  if (!user.logined) {
    newRoutes = authFilter(newRoutes)
  }

  if (!user.account_type) {
    const entryRoute = newRoutes.find(route => route.name === 'Entry')
    if (!entryRoute.children.find(route => route.name === 'Register')) {
      entryRoute
        .children
        .push({
          path: '/register',
          name: 'Register',
          meta: { layout: 'simplify' },
          component: () => import('@/screens/Entry/components/RegisterForm')
        })
    }
  }
  router.addRoutes(newRoutes)
}
