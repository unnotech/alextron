// https://github.com/ElemeFE/element/blob/dev/src/utils/dom.js
export function hasClass (el, cls) {
  if (!el || !cls) return false
  if (cls.indexOf(' ') !== -1) throw new Error('className should not contain space.')
  return el.classList.contains(cls)
}

export function addClass (el, cls) {
  if (!el) return
  var classes = (cls || '').split(' ')

  for (var i = 0, j = classes.length; i < j; i++) {
    var clsName = classes[i]
    if (!clsName) continue
    el.classList.add(clsName)
  }
}

export function removeClass (el, cls) {
  if (!el || !cls) return
  var classes = cls.split(' ')
  for (var i = 0, j = classes.length; i < j; i++) {
    var clsName = classes[i]
    if (!clsName) continue

    el.classList.remove(clsName)
  }
};
