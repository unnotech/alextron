import { localStorageKey } from '@/config/jwtToken'
import getSpecificPlayGroupSetting from '@/config/specificPlay'

export const formatPlayGroup = (raw, formatting, gameType) => {
  let sections = []
  formatting.forEach(format => {
    const playGroups = []
    const nameHash = {}
    const aliasHash = {}
    const aliasGroup = []
    format.grp_code.forEach(code => {
      const matchedGroup = raw.find(group => group.code === code)

      if (matchedGroup) {
        let groupName = matchedGroup.display_name
        matchedGroup.plays.forEach(play => {
          if (play.alias) {
            if (aliasHash[play.alias]) {
              aliasHash[play.alias].plays.push(play)
            } else {
              let group = { ...matchedGroup, plays: [play], alias: play.alias }
              let setting = getSpecificPlayGroupSetting(group.code, gameType)
              if (setting) {
                group.setting = setting
              }
              aliasHash[play.alias] = group
              aliasGroup.push(group)
            }
          } else {
            if (nameHash[groupName]) {
              nameHash[groupName].plays.push(play)
            } else {
              let group = { ...matchedGroup, plays: [play] }
              let setting = getSpecificPlayGroupSetting(group.code, gameType)
              if (setting) {
                group.setting = setting
              }
              nameHash[groupName] = group
              playGroups.push(group)
            }
          }
        })
      }
    })
    if (aliasGroup.length) {
      sections.push({
        groupCol: format.grp_col,
        playCol: format.play_col,
        playGroups: aliasHash,
        aliases: aliasGroup.map(g => g.alias)
      })
    }
    if (playGroups.length) {
      sections.push({
        groupCol: format.grp_col,
        playCol: format.play_col,
        playGroups: playGroups
      })
    }
  })
  return sections
}

export function getBearerToken (type, field = 'access_token') {
  let setting = localStorage.getItem(type)
  if (setting) {
    setting = JSON.parse(setting)
    return setting[field]
  }
  return ''
}

export function getJWTToken (type) {
  let setting = localStorage.getItem(localStorageKey[type])
  if (setting) {
    setting = JSON.parse(setting)
    if (new Date().getTime() / 1000 < setting.expire) {
      return setting.token
    }
  }
  return ''
}

export function saveLastGameData (lastGameData) {
  let lastGameDataStr = localStorage.getItem('lastGameData')
  let lastGameDataObj
  if (lastGameDataStr) {
    lastGameDataObj = JSON.parse(lastGameDataStr)
  }
  if (lastGameDataObj) {
    lastGameDataObj.pc = lastGameData
  } else {
    lastGameDataObj = { pc: lastGameData }
  }
  localStorage.setItem('lastGameData', JSON.stringify(lastGameDataObj))
}

export function getLastGameData () {
  let lastGameDataStr = localStorage.getItem('lastGameData')
  let lastGameData
  if (lastGameDataStr) {
    lastGameData = JSON.parse(lastGameDataStr).pc
  }
  if (lastGameData) {
    lastGameData = {
      lastGame: lastGameData.lastGame || '',
      lastCategory: lastGameData.lastCategory || {}
    }
  } else {
    return {
      lastGame: '',
      lastCategory: {}
    }
  }
  return lastGameData
}
