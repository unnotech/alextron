import store from '@/store'
import { ObservableWebsocket } from './prototype'

const wsHost = `${process.env.wsEagleHost}/chat/`

function onMessage (data) {
  switch (data.type) {
    case 'initial-data':
      store.commit('chatroom/initChatroom', data)
      store.commit('chatroom/setStatus', 'ready')
      break
    case 'message':
    case 'image':
    case 'betrecord-sharing':
    case 'prediction-sharing':
      if (data.type === 'image' && data.sender.username === store.state.user.username) break
      store.commit('chatroom/appendMessage', [data])
      break
    case 'event':
      switch (data.code) {
        case 'chatroom_disabled':
          store.commit('chatroom/setStatus', 'disabled')
          break
      }
  }
}

class EagleWs extends ObservableWebsocket {
  constructor (token, roomId) {
    super(`${wsHost}?token=${token}&room_id=${roomId}`)
    this.roomId = roomId
    this.onMessage = onMessage
    this.connect()
  }

  // 未完成: 換房間、失敗重新連線機制
  // ...
  send ({ type, content }) {
    this.ws.next({
      'command': 'send',
      'type': type,
      'receiver': this.roomId,
      'content': content
    })
  }

  close () {
    this.close()
  }
}

export { EagleWs }
