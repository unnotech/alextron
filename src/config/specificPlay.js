
const specificPlayGroups = {
  'cqlf': [{ // 连码
    suffix: '_pg_wc_wc',
    component: 'WithCode',
    options: Array.from(Array(20).keys()).map(item => item + 1),
    cols: 5
  }],
  'gdklsf': [{ // 连码
    suffix: '_pg_wc_wc',
    component: 'WithCode',
    options: Array.from(Array(20).keys()).map(item => item + 1),
    cols: 5
  }],
  'mark6': [{ // 连码
    suffix: '_pg_withcode',
    component: 'WithCode',
    options: Array.from(Array(49).keys()).map(item => item + 1),
    cols: 7
  }, { // 合肖
    suffix: '_pg_shxiao_spczdc',
    component: 'hklPgShxiaoSpczdc',
    options: Array.from(Array(12).keys()).map(item => item + 1),
    cols: 2
  }, {// 二连肖
    suffix: '_pg_exlzdc2',
    component: 'hklPgExl',
    cols: 2
  }, { // 三连肖
    suffix: '_pg_exlzdc3',
    component: 'hklPgExl',
    cols: 2
  }, { // 四连肖
    suffix: '_pg_exlzdc4',
    component: 'hklPgExl',
    cols: 2
  }, { // 五连肖
    suffix: '_pg_exlzdc5',
    component: 'hklPgExl',
    cols: 2
  }, { // 二连尾
    suffix: '_pg_exltail2',
    component: 'hklPgExl',
    cols: 2
  }, { // 三连尾
    suffix: '_pg_exltail3',
    component: 'hklPgExl',
    cols: 2
  }, { // 四连尾
    suffix: '_pg_exltail4',
    component: 'hklPgExl',
    cols: 2
  }, { // 五连尾
    suffix: '_pg_exltail5',
    component: 'hklPgExl',
    cols: 2
  }, { // 自选不中
    suffix: '_pg_ntinfvr_num',
    component: 'hklPgNtinfvrNum',
    options: Array.from(Array(49).keys()).map(item => item + 1),
    cols: 7
  }],
  '11pick5': [{ // 连码
    suffix: '_pg_wc_wc',
    component: 'common',
    options: Array.from(Array(11).keys()),
    cols: 6,
    transpose: true
  }, { // 直选
    suffix: '_pg_seq_seq',
    component: 'gd11x5Seq',
    options: Array.from(Array(11).keys()),
    cols: 6
  }],
  'fc3d': [{ // 二字定位
    suffix: '_pg_2df',
    component: 'fc3dPg2df',
    options: Array.from(Array(10).keys()).map(item => item),
    cols: 5
  }, { // 三字定位
    suffix: '_pg_3df',
    component: 'fc3dPg2df',
    options: Array.from(Array(10).keys()).map(item => item),
    cols: 5
  }, { // 组选三
    suffix: '_pg_pic',
    component: 'fc3dPgIc',
    options: Array.from(Array(10).keys()),
    cols: 2
  }, { // 组选六
    suffix: '_pg_msic',
    component: 'fc3dPgIc',
    options: Array.from(Array(10).keys()),
    cols: 2
  }],
  'twwfc_pg_wc': { // 台灣五分彩 組選
    component: 'twwfcWc',
    options: Array.from(Array(10).keys()),
    cols: 5,
    transpose: true
  },
  'cstw5fc_pg_wc': {
    component: 'twwfcWc',
    options: Array.from(Array(10).keys()),
    cols: 5,
    transpose: true
  },
  'twwfc_pg_seq': { // 台灣五分彩 直選
    component: 'twwfcSeq',
    options: Array.from(Array(10).keys()),
    cols: 5
  },
  'cstw5fc_pg_seq': {
    component: 'twwfcSeq',
    options: Array.from(Array(10).keys()),
    cols: 5
  }
}

export default function (groupCode, type) {
  if (!specificPlayGroups[groupCode]) {
    if (specificPlayGroups[type]) {
      return specificPlayGroups[type].find(o => groupCode.endsWith(o.suffix))
    } else {
      return undefined
    }
  }
  return specificPlayGroups[groupCode]
}
