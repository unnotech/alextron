export const msnnTable =
[
  {
    displayName: '时间',
    key: 'schedule_result'
  },
  {
    displayName: '期数',
    key: 'issue_number'
  },
  {
    displayName: '开奖号码',
    key: 'result_str'
  },
  {
    displayName: '庄闲 (背景有颜色代表闲家赢)',
    subHeads: [
      {
        displayName: '庄',
        key: 'banker'
      },
      {
        displayName: '闲一',
        key: 'player_1'
      },
      {
        displayName: '闲二',
        key: 'player_2'
      },
      {
        displayName: '闲三',
        key: 'player_3'
      },
      {
        displayName: '闲四',
        key: 'player_4'
      },
      {
        displayName: '闲五',
        key: 'player_5'
      }
    ]
  }
]
