export const hklTable =
[
  {
    displayName: '时间',
    key: 'schedule_result'
  },
  {
    displayName: '期数',
    key: 'issue_number'
  },
  {
    displayName: '',
    buttons: [
      {
        displayName: '号码',
        show: 'number'
      },
      {
        displayName: '大小',
        show: 'thanSize'
      },
      {
        displayName: '单双',
        show: 'oddEven'
      },
      {
        displayName: '合大小',
        show: 'ballOfSumThanSize'
      },
      {
        displayName: '合单双',
        show: 'ballOfSumOddEven'
      },
      {
        displayName: '尾大小',
        show: 'tailThanSize'
      }
    ]
  },
  {
    displayName: '总和',
    subHeads: [
      {
        displayName: '号码',
        key: 'sum_of_ball'
      },
      {
        displayName: '单双',
        key: 'sum_of_ball_odd_even'
      },
      {
        displayName: '大小',
        key: 'sum_of_ball_than_size'
      },
      {
        displayName: '七色波',
        key: 'seven_color_wavelength'
      }
    ]
  }
]
