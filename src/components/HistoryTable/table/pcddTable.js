export const pcddTable =
[
  {
    displayName: '时间',
    key: 'schedule_result'
  },
  {
    displayName: '期数',
    key: 'issue_number'
  },
  {
    displayName: '开奖号码',
    key: 'result_str'
  },
  {
    displayName: '总和',
    subHeads: [
      {
        displayName: '号码',
        key: 'sum_of_ball'
      },
      {
        displayName: '大小',
        key: 'sum_of_ball_than_size'
      },
      {
        displayName: '单双',
        key: 'sum_of_ball_odd_even'
      },
      {
        displayName: '色波',
        key: 'sum_of_ball_color_wavelength'
      },
      {
        displayName: '极值',
        key: 'sum_of_ball_great_tiny'
      }
    ]
  }
]
