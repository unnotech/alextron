const resultFilter = (val) => {
  switch (val) {
    case 'dragon':
      return '龙'
    case 'tiger':
      return '虎'
    case 'bigger':
      return '大'
    case 'smaller':
      return '小'
    case 'great':
      return '极大'
    case 'tiny':
      return '极小'
    case 'outOfDefinition':
      return '无极值'
    case 'odd':
      return '单'
    case 'even':
      return '双'
    case 'straight':
      return '顺子'
    case 'half_straight':
      return '半顺'
    case 'misc_six':
      return '杂六'
    case 'pair':
      return '对子'
    case 'leopard':
      return '豹子'
    case 'blue':
      return '蓝波'
    case 'red':
      return '红波'
    case 'green':
      return '绿波'
    case 'equal':
      return '和'
    case 'gold':
      return '金'
    case 'wood':
      return '木'
    case 'water':
      return '水'
    case 'fire':
      return '火'
    case 'earth':
      return '土'
    case 'front_part_more':
      return '前多'
    case 'rear_part_more':
      return '后多'
    case 'odd_more':
      return '单多'
    case 'even_more':
      return '双多'
    case 'prime':
      return '质'
    case 'composite':
      return '合'
    default:
      return val
  }
}

export { resultFilter }
