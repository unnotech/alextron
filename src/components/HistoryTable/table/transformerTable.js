export const transformerTable =
[
  {
    displayName: '时间',
    key: 'schedule_result'
  },
  {
    displayName: '期数',
    key: 'issue_number'
  },
  {
    displayName: '',
    buttons: [
      { displayName: '显示号码', show: 'number' },
      { displayName: '显示大小', show: 'thanSize' },
      { displayName: '显示单双', show: 'oddEven' }
    ]
  },
  {
    displayName: '冠亚和',
    subHeads: [
      {
        displayName: '号码',
        key: 'sum_of_1st_2st'
      },
      {
        displayName: '大小',
        key: 'sum_of_1st_2st_than_size'
      },
      {
        displayName: '单双',
        key: 'sum_of_1st_2st_odd_even'
      }
    ]
  },
  {
    displayName: ' 1 - 5 龙 虎 ',
    subHeads: [
      {
        displayName: '',
        key: 'dragon_tiger_1_10'
      },
      {
        displayName: '',
        key: 'dragon_tiger_2_9'
      },
      {
        displayName: '',
        key: 'dragon_tiger_3_8'
      },
      {
        displayName: '',
        key: 'dragon_tiger_4_7'
      },
      {
        displayName: '',
        key: 'dragon_tiger_5_6'
      }
    ]
  }
]
