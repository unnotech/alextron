const positionFields = [
  {
    displayName: '回摆',
    subFields: [
      {
        displayName: '反向',
        key: 'backward'
      },
      {
        displayName: '重号',
        key: 'repeat'
      },
      {
        displayName: '正向',
        key: 'forward'
      }
    ]
  },
  {
    displayName: '单双',
    subFields: [
      {
        displayName: '单',
        key: 'odd'
      },
      {
        displayName: '双',
        key: 'even'
      }
    ]
  },
  {
    displayName: '大小',
    subFields: [
      {
        displayName: '大',
        key: 'bigger'
      },
      {
        displayName: '小',
        key: 'small'
      }
    ]
  }
]

const numberFields = [
  {
    displayName: '回摆',
    subFields: [
      {
        displayName: '反向',
        key: 'backward'
      },
      {
        displayName: '重号',
        key: 'repeat'
      },
      {
        displayName: '正向',
        key: 'forward'
      }
    ]
  },
  {
    displayName: '前后',
    subFields: [
      {
        displayName: '前',
        key: 'front'
      },
      {
        displayName: '后',
        key: 'behind'
      }
    ]
  }
]

const fieldsMap = {
  position: positionFields,
  number: numberFields,
  sum: positionFields
}

export { fieldsMap }
