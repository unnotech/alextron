import { Subject, defer, merge } from 'rxjs'
import { shareReplay } from 'rxjs/operators'

class FetchData {
  constructor (request) {
    this._subject$ = new Subject()
    this._request = request

    // 訂閱後會請求一次ＡＰＩ
    // 之後每次訂閱都會緩存上次的的資料
    // refetch調用後 每個訂閱都會接收到新資料
    const initFetch$ = defer(() => this.refetch(false))

    this._data$ = merge(
      initFetch$,
      this._subject$.asObservable()
    ).pipe(
      shareReplay({ refCount: true, bufferSize: 1 }) // 最多推一次資料
    )
  }

  get observable () {
    return this._data$
  }

  subscribe (func) {
    this._data$.subscribe(func)
  }

  // emit = true 重新抓資料
  refetch (emit = true) {
    return this._request().then(data => {
      emit && this._subject$.next(data)
      return data
    })
  }
}

export { FetchData }
