import axios from 'axios'
import { HTTP_ERROR, report } from '@/report'
import store from '@/store'

const axiosCampaign = axios.create({
  baseURL: process.env.HOST + '/campaign'
})

axiosCampaign.interceptors.request.use((config) => {
  let token = store.state.token.fox
  if (token) {
    config.headers.common['Authorization'] = 'JWT ' + token
  }
  return config
}, function (error) {
  return Promise.reject(error)
})

axiosCampaign.interceptors.response.use(res => {
  return res.data
}, (error) => {
  report({
    type: HTTP_ERROR,
    error
  })
  return Promise.reject(error.response)
})

export default {
  fetchRechargeEnvelop: () => axiosCampaign.get('/envelope/recharge_envelope/criteria/'),

  snatchRechargeEnvelop: () => axiosCampaign.get('/envelope/recharge_envelope/')
}
