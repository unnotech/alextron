import axios from 'axios'
import Vue from 'vue'
import qs from 'qs'
import { HTTP_ERROR, AUTH_ERROR, report } from '@/report'
import store from '@/store'
import { router } from '@/router'
import { apiPath as tokenPath } from '@/config/jwtToken'

let url = window.location.href
let search = url.match(/\?([^/#]*)/)
const params = search ? qs.parse(search[1]) : {}
const pollingApi = ['/v2/member/result/']

if (params.f) {
  Vue.cookie.set('referral_id', params.f)
}

const axiosGhost = axios.create({
  baseURL: process.env.HOST
})

const toHomeAndLogin = function (router) {
  store.dispatch('user/reset')
  router.push({
    path: '/'
  })
}

axiosGhost.defaults.headers.common['x-r'] = params.r || ''
axiosGhost.interceptors.request.use((config) => {
  let token = store.state.token.ghost
  if (token) {
    config.headers.common['Authorization'] = 'Bearer ' + token
  }
  return config
}, function (error) {
  return Promise.reject(error)
})

axiosGhost.interceptors.response.use(res => {
  let responseData = res.data
  const fromVenom = res.config.url.includes(process.env.venomSocket.replace(/"/g, ''))
  if (fromVenom) {
    return responseData
  }
  if (responseData.code === 2000) {
    return responseData.data
  } else if (responseData.code === 9001) {
    return responseData
  } else {
    if (responseData.code === 9007) {
      if (!pollingApi.some(url => res.config.url.indexOf(url) !== -1)) { // 忽略輪詢api
        report({
          type: AUTH_ERROR,
          url: res.config.url,
          msg: '9007身份认证信息未提供'
        })
        toHomeAndLogin(router)
      }
    }
    return Promise.reject(responseData)
  }
}, (error) => {
  report({
    type: HTTP_ERROR,
    error
  })
  return Promise.reject(error)
})

export default {
  getToken: token => axiosGhost.post('/member/login/refresh/', {
    refresh_token: token
  }),

  sendHeartBeat: () => axiosGhost.get('/v2/member/heart_beat/'),

  getHomePage: () => axiosGhost.get('/v2/member/website/home-page/?platform=pc'),

  getAnnouncements: () => axiosGhost.get('/v2/member/announcements/?platform=pc'),

  getPromotions: () => axiosGhost.get('/v2/member/promotion/'),

  fetchGames: () => axiosGhost.get('/v2/member/game/'),

  fetchGamesDetail: () => axiosGhost.get('/v2/member/game/?platform=0&extras=categories,playpositions'),

  fetchPlayGroups: (categoryId) => axiosGhost.get(`/v2/member/game/playgroup/?category=${categoryId}`),

  fetchUser: () => axiosGhost.get('/v2/members'),

  updateUser: (userId, payload) => axiosGhost.put(`/v2/members/${userId}/`, payload),

  fetchJWTToken: type => {
    return axiosGhost.get('/jwt/?service_type=' + tokenPath[type]).then(res => {
      return {
        expire: res.expire || res.expires_at,
        token: res[tokenPath[type] + '_token'] || res.token
      }
    })
  },

  fetchSchedule: (gameId, gameCode) => axiosGhost.get(`/v2/member/game/schedule/?&game=${gameId}&game_code=${gameCode}`),

  fetchGameResult: (gameId) => axiosGhost.get(`/v2/member/result/?game=${gameId}&opt_expand=next`),

  fetchCaptcha: () => axiosGhost.get('/v2/member/verification_code/')
    .then(data => {
      data.captcha_src = process.env.HOST + data.captcha_src
      return data
    }),

  login: user => axiosGhost.post('/member/login/', qs.stringify(user)),

  trial: data => {
    let payload = {
      account_type: 0,
      ...((data && data.verification_code_0 && data.verification_code_0) && {
        verification_code_0: data.verification_code_0,
        verification_code_1: data.verification_code_1
      })
    }
    return axiosGhost.post('/v2/member/register/trial/', qs.stringify(payload))
  },

  register: (user, r) => axiosGhost.post('/v2/member/register/', qs.stringify(user), {
    withCredentials: true,
    headers: {
      'x-r': r
    }
  }).then(res => {
    axiosGhost.defaults.headers.common['x-r'] = r
    return Promise.resolve(res)
  })
}
