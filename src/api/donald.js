import axios from 'axios'
import store from '@/store'
import { HTTP_ERROR, report } from '@/report'

export const axiosDonald = axios.create({
  baseURL: process.env.donaldHost
})

axiosDonald.interceptors.request.use((config) => {
  let token = store.state.token.donald
  if (token) {
    config.headers.common['Authorization'] = 'JWT ' + token
  }
  return config
}, function (error) {
  return Promise.reject(error)
})

axiosDonald.interceptors.response.use(res => {
  return res.data
}, (error) => {
  report({
    type: HTTP_ERROR,
    error
  })
  return Promise.reject(error.response)
})

export default {
  getOverview: () => axiosDonald.get('/overview/'),

  getSetting: () => axiosDonald.get('/preferences/'),

  getIncomeRecord: params => axiosDonald.get('/transactions/', params)
}
