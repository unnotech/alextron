import axios from 'axios'
import store from '@/store'
import { HTTP_ERROR, report } from '@/report'

export const axiosFox = axios.create({
  baseURL: process.env.foxHost
})

axiosFox.interceptors.request.use((config) => {
  let token = store.state.token.fox
  if (token) {
    config.headers.common['Authorization'] = 'JWT ' + token
  }
  return config
}, function (error) {
  return Promise.reject(error)
})

axiosFox.interceptors.response.use(res => {
  return res.data
}, (error) => {
  report({
    type: HTTP_ERROR,
    error
  })
  return Promise.reject(error.response)
})

export default {
  fetchHistory: option => {
    let url = `/result/history/?limit=30`
    Object.keys(option).forEach(key => {
      if (option[key]) {
        url += `&${key}=${option[key]}`
      }
    })
    return axiosFox.get(url)
  },

  fetchStatistic: code => axiosFox.get(`/result/statistic/?game_code=${code}`),

  fetchTrendChart: params => axiosFox.get('/result/trend_chart/', { params })
}
