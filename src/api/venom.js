import axios from 'axios'
import store from '@/store'
import { HTTP_ERROR, report } from '@/report'

const axiosVenom = axios.create({
  baseURL: process.env.venomHost
})

axiosVenom.interceptors.request.use((config) => {
  let token = store.state.token.venom
  if (token) {
    config.headers.common['Authorization'] = 'JWT ' + token
  }
  return config
}, function (error) {
  return Promise.reject(error)
})

axiosVenom.interceptors.response.use(res => {
  return res.data
}, (error) => {
  report({
    type: HTTP_ERROR,
    error
  })
  return Promise.reject(error.response)
})

export default {
  fetchServiceEmoji: () => axiosVenom.get('/chat/emoji/'),
  serviceTrial: () => axiosVenom.patch('/user/mark_as_trial/')
}
