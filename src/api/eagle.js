import axios from 'axios'
import store from '@/store'
import { FetchData } from './utils/fetchData'
import { HTTP_ERROR, report } from '@/report'

export const axiosEagle = axios.create({
  baseURL: process.env.eagleHost
})

axiosEagle.interceptors.request.use((config) => {
  let token = store.state.token.eagle
  if (token) {
    config.headers.common['Authorization'] = 'JWT ' + token
  }
  return config
}, function (error) {
  return Promise.reject(error)
})

axiosEagle.interceptors.response.use(res => {
  return res.data
}, (error) => {
  report({
    type: HTTP_ERROR,
    error
  })
  return Promise.reject(error.response)
})

export default {
  sendImg: data => axiosEagle.post('/v1/attachment/', data),

  fetchStickers: new FetchData(() => axiosEagle.get('/v1/stickers/')),

  /**
   * 禁言之設定與取消、管理員之加入與刪除
   * @param {string} data.action - ban, unban, add_manager, remove_manager
   * @param {string} data.username
   * @param {number} data.duration - for action ban
   */
  controlChatMember: (rommId, action, data) => axiosEagle.post(`/v1/rooms/${rommId}/${action}/`, data),

  /**
   * 禁言名單
   * @param {number} roomId
   */
  fetchBannedList: roomId => axiosEagle.get('/v1/room-banned-users/', {
    params: {
      room_id: roomId
    }
  }),

  /**
   * 黑名單列表
   * @param {number} roomId
   */
  fetchBlacklistedList: roomId => axiosEagle.get('/v1/room-blacklisted-users/', {
    params: {
      room_id: roomId
    }
  }),

  /**
   * 取得遊戲聊天室 mapping
   */
  fetchGameRoomList: params => axiosEagle.get('/v1/games/', params),

  /**
   * 取得聊天室會員資料
   */
  fetchChatRoomUserInfo: username => axiosEagle.get(`/v1/users/${username}/`),

  /**
   * 使用者詳情
   * @param {string} username
   * @param {number} roomId
   */
  fetchUserDetail: (username, roomId) => axiosEagle.get(`/v1/users/${username}/details/?room_id=${roomId}`),

  /**
   * 修改聊天室會員資料
   */
  updateChatRoomUserInfo: (username, data) => axiosEagle.patch(`/v1/users/${username}/`, data),

  /**
   * 顯示關注會員名單
   */
  fetchFolloweeList: () => axiosEagle.get('/v1/users/followee-list/'),

  /**
   * 關注/取消關注會員
   * @param {*} username
   */
  toggleFollowee: username => axiosEagle.post('/v1/users/toggle-follow/', { followee: username }),

  fetchFilterSettings: username => axiosEagle.get(`/v1/users/${username}/?fields=filter_followee,game_settings,platform=pc`)
}
