/**
 * {
 *   [GameType]: {
 *      [ChartType]: [{
*          [label],
*          [key]
 *      }]
 *   }
 * }
 */
const optionMap = {
  'pk10': {
    position: [
      {
        'label': '冠军',
        'key': '1'
      }, {
        'label': '亚军',
        'key': '2'
      }, {
        'label': '第三名',
        'key': '3'
      }, {
        'label': '第四名',
        'key': '4'
      }, {
        'label': '第五名',
        'key': '5'
      }, {
        'label': '第六名',
        'key': '6'
      }, {
        'label': '第七名',
        'key': '7'
      }, {
        'label': '第八名',
        'key': '8'
      }, {
        'label': '第九名',
        'key': '9'
      }, {
        'label': '第十名',
        'key': '10'
      }
    ],
    number: [
      {
        'label': '号码 1',
        'key': '1'
      }, {
        'label': '号码 2',
        'key': '2'
      }, {
        'label': '号码 3',
        'key': '3'
      }, {
        'label': '号码 4',
        'key': '4'
      }, {
        'label': '号码 5',
        'key': '5'
      }, {
        'label': '号码 6',
        'key': '6'
      }, {
        'label': '号码 7',
        'key': '7'
      }, {
        'label': '号码 8',
        'key': '8'
      }, {
        'label': '号码 9',
        'key': '9'
      }, {
        'label': '号码 10',
        'key': '10'
      }
    ]
  },
  'time-lottery': {
    position: [
      {
        label: '第一球',
        key: '1'
      },
      {
        label: '第二球',
        key: '2'
      },
      {
        label: '第三球',
        key: '3'
      },
      {
        label: '第四球',
        key: '4'
      },
      {
        label: '第五球',
        key: '5'
      }
    ]
  },
  'dd': {
    position: [
      {
        label: '第一球',
        key: '1'
      },
      {
        label: '第二球',
        key: '2'
      },
      {
        label: '第三球',
        key: '3'
      }
    ]
  },
  'gdklsf': { // 广东快乐十分
    position: [
      {
        label: '第一球',
        key: '1'
      },
      {
        label: '第二球',
        key: '2'
      },
      {
        label: '第三球',
        key: '3'
      },
      {
        label: '第四球',
        key: '4'
      },
      {
        label: '第五球',
        key: '5'
      },
      {
        label: '第六球',
        key: '6'
      },
      {
        label: '第七球',
        key: '7'
      },
      {
        label: '第八球',
        key: '8'
      }
    ]
  },
  '11pick5': {
    position: [
      {
        label: '第一球',
        key: '1'
      },
      {
        label: '第二球',
        key: '2'
      },
      {
        label: '第三球',
        key: '3'
      },
      {
        label: '第四球',
        key: '4'
      },
      {
        label: '第五球',
        key: '5'
      }
    ]
  },
  'k3': {
    sum: []
  }
}

export { optionMap }

const ballMap = {
  'pk10': {
    from: 1,
    to: 10
  },
  'time-lottery': {
    from: 0,
    to: 9
  },
  'dd': {
    from: 0,
    to: 9
  },
  'gdklsf': { // 广东快乐十分
    from: 1,
    to: 20
  },
  '11pick5': {
    from: 1,
    to: 11
  },
  'k3': {
    from: 3,
    to: 18
  }
}

export { ballMap }

const typeFilter = type => {
  switch (type) {
    case 'position':
      return '位置走势'
    case 'number':
      return '号码走势'
    case 'sum':
      return '总和走势'
  }
}

export { typeFilter }
