import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import token from './modules/token'
import user from './modules/user'
import game from './modules/game'
import chatroom from './modules/chatroom'
import announcement from './modules/announcement'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    app,
    token,
    user,
    game,
    chatroom,
    announcement
  },
  getters
})
