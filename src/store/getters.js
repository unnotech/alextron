const getters = {
  activeLink: state => state.app.navbar.active
}
export default getters
