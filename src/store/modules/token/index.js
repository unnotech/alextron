import { getBearerToken, getJWTToken } from '@/utils'
import { localStorageKey } from '@/config/jwtToken'
import ghost from '@/api/ghost'
const GHOST_TOKEN = 'ghost_token'

export default {
  namespaced: true,
  state: {
    eagle: getJWTToken('eagle'),
    venom: getJWTToken('venom'),
    donald: getJWTToken('donald'),
    fox: getJWTToken('fox'),
    ghost: getBearerToken(GHOST_TOKEN)
  },
  mutations: {
    add: (state, { type, setting }) => {
      localStorage.setItem(localStorageKey[type], JSON.stringify(setting))
      if (type === 'ghost') {
        state[type] = setting.access_token
      } else {
        state[type] = setting.token
      }
    },
    clear: (state, type) => {
      if (type !== 'all') {
        localStorage.removeItem(localStorageKey[type])
        state[type] = ''
      } else {
        Object.keys(state).forEach(key => {
          localStorage.removeItem(localStorageKey[type])
          state[key] = ''
        })
      }
    }
  },
  actions: {
    fetch: ({ commit }, type) => {
      ghost.fetchJWTToken(type).then(setting => {
        commit('add', {
          type: type,
          setting // { token, expire }
        })
      }).catch(() => {})
    }
  }
}
