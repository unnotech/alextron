export default {
  namespaced: true,
  state: {
    ws: null,
    status: 'pending',
    availableStatus: ['pending', 'ready', 'disabled'],
    maximumMessageCount: 100,
    messages: []
  },
  mutations: {
    setWs (state, ws) {
      state.ws = ws
    },
    setStatus (state, status) {
      if (!state.availableStatus.includes(status)) {
        throw new Error(`Invalid status "${status}" for mutation 'chatroom/setStatus'.`)
      }
      state.status = status
    },
    initChatroom (state, payload) {
      state.messages = payload.recent_messages
    },
    appendMessage (state, payload) {
      state.messages = state.messages.concat(payload).splice(-1 * state.maximumMessageCount)
    },
    deleteMessage (state, { id }) {
      const idx = state.messages.findIndex(message => message.id === id)
      state.messages.splice(idx, 1)
    }
  }
}
