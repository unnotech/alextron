import ghost from '@/api/ghost'

export default {
  namespaced: true,
  state: {
    announcements: [],
    dialog: {
      visible: false
    }
  },
  mutations: {
    init: (state, data) => {
      state.announcements = data
    },
    showDialog: (state) => {
      state.dialog.visible = true
    },
    hideDialog: (state) => {
      state.dialog.visible = false
    }
  },
  actions: {
    fetch: ({ commit }) => {
      ghost.getAnnouncements().then(
        result => {
          const datas = []
          result.forEach((item) => {
            if (item.platform !== 0) {
              datas.push(item)
            }
          })

          if (datas.length) {
            datas.sort((a, b) => a.rank - b.rank)
            commit('init', datas.map(data => data.announcement))
          }
        }
      ).catch(() => {})
    }
  }
}
