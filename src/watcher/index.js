import store from '@/store'
import { refreshRouter } from '@/router'
import { GhostHeartBeat, GhostTokenChecker } from './helper'

export function watchUserLoginStatus () {
  const heartBeatInterval = new GhostHeartBeat()
  const tokenChecker = new GhostTokenChecker()
  store.watch(
    state => state.user.logined,
    logined => {
      if (logined === true) {
        heartBeatInterval.start()
        tokenChecker.start()
        // get eagle token
        const eagleToken = store.state.token.eagle
        if (!eagleToken) {
          store.dispatch('token/fetch', 'eagle')
        }
        // get fox token
        const foxToken = store.state.token.fox
        if (!foxToken) {
          store.dispatch('token/fetch', 'fox')
        }
      } else {
        heartBeatInterval.clear()
        tokenChecker.clear()
        // store.commit('RESET_MESSAGE_COUNT')

        // if (store.state.ws.eider) {
        //   store.state.ws.eider.closeConnect()
        // }
      }
    }
  )
}

// 動態更新router
export function setDynamicRouting () {
  store.watch(
    state => [state.user.logined, state.app.systemConfig.theme],
    ([logined, theme], [oldLogined, oldTheme]) => {
      if (logined === oldLogined && theme === oldTheme) {
        return
      }
      refreshRouter({ user: store.state.user, theme })
    }
  )
}
