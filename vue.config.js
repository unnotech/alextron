const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const path = require('path')
const webpack = require('webpack')

function resolve (dir) {
  return path.join(__dirname, dir)
}

let env = require('./env')

env = process.env.NODE_ENV === 'production' ? env.prod : env.dev

let companyMap = {
  'hg9q_1': {
    id: 1,
    name: 'cc722'
  },
  '75ue_2': {
    id: 2,
    name: 'fh801'
  },
  'cg8s_3': {
    id: 3,
    name: 'a59'
  },
  '8fn3_4': {
    id: 4,
    name: 'hm7899'
  },
  'ee9m_5': {
    id: 5,
    name: 'baiduwt'
  },
  'knqz_6': {
    id: 6,
    name: 'akw30'
  }
}

let companyInfo = companyMap[process.env.company] || { id: 0, name: 'staging' }

const favicon = (companyInfo.id === 2 || companyInfo.id === 5) ? process.env.company : 'default'

const configureWebpack = {
  resolve: {
    extensions: ['.js', '.vue', '.json', '.css'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': resolve('src')
    }
  },
  plugins: []
}
configureWebpack.plugins.push(
  new webpack.DefinePlugin({
    'process.env': env
  }),
  new FaviconsWebpackPlugin({
    logo: `./src/assets/favicon/${favicon}.png`,
    prefix: 'static/favicons/',
    appName: process.env.SITE_TITLE || 'STAGING',
    title: process.env.SITE_TITLE || 'STAGING',
    icons: {
      android: true,
      appleIcon: true,
      appleStartup: true,
      coast: false,
      favicons: true,
      firefox: false,
      windows: false,
      yandex: false
    }
  }),
  new CopyWebpackPlugin([
    {
      from: path.resolve(__dirname, './static'),
      to: 'static',
      ignore: ['.*']
    }
  ])
)
module.exports = {
  lintOnSave: true,
  pages: {
    index: {
      entry: './src/main.js',
      filename: 'index.html',
      template: 'static/index.html',
      title: env.SITE_TITLE.replace(/"/g, ''),
      companyId: companyInfo.id,
      companyName: companyInfo.name,
      host: process.env.HOST || '',
      inject: true,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true
        // more options:
        // https://github.com/kangax/html-minifier#options-quick-reference
      },
      // necessary to consistently work with multiple chunks via CommonsChunkPlugin
      chunksSortMode: 'dependency'
    },
    app: {
      entry: './src/main.js',
      filename: 'app.html',
      template: 'static/app.html',
      title: env.SITE_TITLE.replace(/"/g, ''),
      companyId: companyInfo.id,
      companyName: companyInfo.name,
      appHost: `https://storage.googleapis.com/lutra/${companyInfo.name}/`, // app storage url
      staticRoot: '/static/app/',
      host: process.env.HOST || '',
      inject: false,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true
        // more options:
        // https://github.com/kangax/html-minifier#options-quick-reference
      },
      chunksSortMode: 'dependency'
    }
  },
  configureWebpack,
  chainWebpack: config => {
    config.plugins.delete('prefetch-index')

    config.plugin('define').tap(definitions => {
      Object.assign(definitions[0]['process.env'], env)
      return definitions
    })
  },
  devServer: {
    proxy: {
      '/api-ghost': {
        target: 'https://staging-api.h9339.com',
        changeOrigin: true,
        pathRewrite: {
          '^/api-ghost': ''
        }
      },
      '/api-eagle': {
        target: 'http://staging-eagle.h9339.com',
        changeOrigin: true,
        pathRewrite: {
          '^/api-eagle': ''
        }
      },
      '/api-donald': {
        target: 'https://staging-api2.h9339.com/donald',
        changeOrigin: true,
        pathRewrite: {
          '^/api-donald': ''
        }
      }
    },
    disableHostCheck: true,
    port: 8080,
    overlay: {
      warnings: true,
      errors: true
    }
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: '@import "@/style/vars.scss";'
      }
    }
  },
  pluginOptions: {},
  assetsDir: 'static/'
}
